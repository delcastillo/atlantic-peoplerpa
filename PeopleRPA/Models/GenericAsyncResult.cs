﻿using System;

namespace Models {
    internal class GenericAsyncResult {
        public string ResultStr { get; set; }
        public bool ResultBool { get; set; }
        public string Detail { get; set; }
        public Exception Exception { get; set; }
    }
}
