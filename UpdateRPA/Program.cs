﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateRPA {
    class Program {
        static void Main(string[] args) {
            ChromeOptions options = new ChromeOptions();
            //options.AddArguments("user-data-dir=" + "%localappdata%\\Google\\Chrome\\User Data\\Profile 1");
            //options.AddUserProfilePreference("download.default_directory", "%UserProfile%\\Downloads");
            //options.AddUserProfilePreference("download.prompt_for_download", false);            
            //options.BinaryLocation = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
            var driver = new ChromeDriver(options);
            driver.Navigate().GoToUrl("http://localhost:4200/admin");
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            try {
                foreach (DataRow item in GetDataTableFromExcel().Rows) {
                    string Last_name = item["Segundo Nombre"].ToString();
                    string Sure = item["Estimado asegurado"].ToString();
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                    driver.FindElement(By.Name("filter")).Clear();
                    driver.FindElement(By.Name("filter")).SendKeys(item["No. Identificacion"].ToString());
                    driver.FindElement(By.Name("edit")).Click();
                    System.Threading.Thread.Sleep(2000);
                    if (driver.FindElement(By.Name("Identification_number")).GetAttribute("value") != item["No. Identificacion"].ToString()) {
                        driver.FindElement(By.Name("Identification_number")).Clear();
                        driver.FindElement(By.Name("Identification_number")).SendKeys(item["No. Identificacion"].ToString());
                    }
                    if (driver.FindElement(By.Name("first_name")).GetAttribute("value") != item["Primer Nombre"].ToString()) {
                        driver.FindElement(By.Name("first_name")).Clear();
                        driver.FindElement(By.Name("first_name")).SendKeys(item["Primer Nombre"].ToString());
                    }
                    if (driver.FindElement(By.Name("Last_name")).GetAttribute("value") != item["Segundo Nombre"].ToString() || driver.FindElement(By.Name("Last_name")).GetAttribute("value") == "") {
                        driver.FindElement(By.Name("Last_name")).Clear();
                        driver.FindElement(By.Name("Last_name")).SendKeys(string.IsNullOrWhiteSpace(Last_name) ? "N/A" : Last_name);
                    }
                    if (driver.FindElement(By.Name("first_surname")).GetAttribute("value") != item["Primer Apellido"].ToString()) {
                        driver.FindElement(By.Name("first_surname")).Clear();
                        driver.FindElement(By.Name("first_surname")).SendKeys(item["Primer Apellido"].ToString());
                    }
                    if (driver.FindElement(By.Name("Last_surname")).GetAttribute("value") != item["Segundo Apellido"].ToString()) {
                        driver.FindElement(By.Name("Last_surname")).Clear();
                        driver.FindElement(By.Name("Last_surname")).SendKeys(item["Segundo Apellido"].ToString());
                    }
                    if (driver.FindElement(By.Name("Number_contact")).GetAttribute("value") != item["No. Contacto"].ToString()) {
                        driver.FindElement(By.Name("Number_contact")).Clear();
                        driver.FindElement(By.Name("Number_contact")).SendKeys(item["No. Contacto"].ToString());
                    }
                    if (driver.FindElement(By.Name("email")).GetAttribute("value") != item["Email"].ToString()) {
                        driver.FindElement(By.Name("email")).Clear();
                        driver.FindElement(By.Name("email")).SendKeys(item["Email"].ToString());
                    }
                    if (driver.FindElement(By.Name("birth")).GetAttribute("value") != item["Fecha nacimiento"].ToString()) {
                        driver.FindElement(By.Name("birth")).Clear();
                        driver.FindElement(By.Name("birth")).SendKeys(item["Fecha nacimiento"].ToString());
                    }
                    if (driver.FindElement(By.Name("Sure")).GetAttribute("value") != item["Estimado asegurado"].ToString()) {
                        driver.FindElement(By.Name("Sure")).Clear();
                        driver.FindElement(By.Name("Sure")).SendKeys(Sure.Any(Char.IsDigit) ? Sure : Sure.Replace("-", "0"));
                    }
                    driver.FindElement(By.Name("editPerson")).Click();
                    System.Threading.Thread.Sleep(2000);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex);
            }
        }

        public static DataTable GetDataTableFromExcel(bool hasHeader = true) {
            using (var pck = new OfficeOpenXml.ExcelPackage()) {
                using (var stream = File.OpenRead("C:\\Users\\delcasyg\\Downloads\\anexo2-prueba-tecnica-rpa - Copia.xlsx")) {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets.First();
                DataTable tbl = new DataTable();
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column]) {
                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                var startRow = hasHeader ? 2 : 1;
                for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++) {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow) {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
                return tbl;
            }
        }
    }
}
